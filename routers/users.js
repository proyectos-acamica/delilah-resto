const express = require('express');
const {index, store} = require('../controllers/users');
const {validateJWT} = require('../middlewares/validar-jwt');
const router = express.Router();


router.get('/',validateJWT, index);
router.post('/', store);

module.exports = router;
