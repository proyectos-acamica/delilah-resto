const express = require('express');
const router = express.Router();
const {validateJWT} = require('../middlewares/validar-jwt');
const {isAdmin} = require('../middlewares/validar-roles');
const {index, store, edit, remove} = require('../controllers/plates');

router.get('/', index);
router.post('/', validateJWT, isAdmin, store);
router.put('/:id', validateJWT, isAdmin, edit);
router.delete('/:id', validateJWT, isAdmin, remove);

module.exports = router;