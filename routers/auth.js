const express = require('express');
const router = express.Router();
const {getToken} = require('../helpers/generar-jwt');
const {getUserByEmail} = require('../models/users');


router.post('/login', async (req, res) =>{
    const {user, password } = req.body

    try {
        const usuario = await getUserByEmail(user);
        if (!usuario || usuario[0].password != password) {
            return res.status(400).json({msg: 'Usuario o contraseña no son correctas.'});
        }
        const token = await getToken (usuario[0].id, usuario[0].roleId)

        res.status(200).json({usuario, token})

        
    } catch (error) {
        res.status(500).json({
            msg: error
        })
    }
});

module.exports = router;