-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2021 a las 01:23:33
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_delilah`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `order_id` int(3) NOT NULL,
  `order_description` varchar(250) COLLATE utf8_bin NOT NULL,
  `order_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `order_total` int(11) NOT NULL DEFAULT 0,
  `order_status_id` int(2) NOT NULL DEFAULT 1,
  `order_payment_id` int(2) NOT NULL,
  `order_user_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`order_id`, `order_description`, `order_time`, `order_total`, `order_status_id`, `order_payment_id`, `order_user_id`) VALUES
(19, '1x Pizza Margherita - Grande 4x Pizza Pepperoni ', '2021-04-03 05:04:02', 45000, 2, 2, 1),
(20, '1x Pizza Hawuaina - Mediana 1x Pizza Margherita - Grande 3x Pizza Pepperoni 1x Pizza Pepperoni - Grande 1x Pizza Pepperoni - Mediana ', '2021-04-03 04:46:39', 95000, 1, 2, 1),
(21, '1x Pizza Hawuaina - Mediana 2x Pizza Margherita - Grande 3x Pizza Pepperoni 1x Pizza Pepperoni - Grande 1x Pizza Pepperoni - Mediana ', '2021-04-03 04:54:46', 120000, 1, 2, 1),
(22, '4x Pizza Hawaiana 2x Pizza Margherita - Grande 9x Pizza Pepperoni 4x Pizza Pepperoni - Mediana ', '2021-04-03 04:55:58', 175000, 1, 2, 1),
(23, '1x Pizza Hawaiana. 1x Pizza Pepperoni. ', '2021-04-03 05:06:12', 10000, 1, 2, 2),
(24, '1x Pizza Hawaiana. 1x Pizza Pepperoni. ', '2021-04-03 22:05:15', 10000, 5, 2, 2),
(25, '1x Pizza Hawaiana. 3x Pizza Margherita - Grande. 1x Pizza Pepperoni. 1x Pizza Pollo con champiñones - Mediana. ', '2021-04-03 21:56:39', 100000, 1, 1, 2),
(26, '1x Pizza Hawaiana. 3x Pizza Margherita - Grande. 1x Pizza Pepperoni. 1x Pizza Pollo con champiñones - Mediana. ', '2021-04-03 22:07:38', 100000, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_has_plates`
--

CREATE TABLE `orders_has_plates` (
  `order_id` int(3) NOT NULL,
  `plate_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `orders_has_plates`
--

INSERT INTO `orders_has_plates` (`order_id`, `plate_id`) VALUES
(19, 1),
(19, 1),
(19, 1),
(19, 2),
(19, 4),
(19, 4),
(19, 1),
(20, 1),
(20, 1),
(20, 10),
(20, 1),
(20, 4),
(20, 12),
(20, 11),
(20, 4),
(21, 1),
(21, 1),
(21, 1),
(21, 10),
(21, 11),
(21, 12),
(21, 4),
(21, 4),
(22, 1),
(22, 1),
(22, 1),
(22, 1),
(22, 1),
(22, 1),
(22, 1),
(22, 1),
(22, 11),
(22, 11),
(22, 1),
(22, 11),
(22, 11),
(22, 2),
(22, 2),
(22, 2),
(22, 2),
(22, 4),
(22, 4),
(23, 1),
(23, 2),
(24, 1),
(24, 2),
(25, 1),
(25, 2),
(25, 4),
(25, 4),
(25, 8),
(25, 4),
(26, 1),
(26, 2),
(26, 4),
(26, 4),
(26, 4),
(26, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `payment_id` int(2) NOT NULL,
  `payment_name` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `payments`
--

INSERT INTO `payments` (`payment_id`, `payment_name`) VALUES
(1, 'Efectivo'),
(2, 'Tarjeta de Credito'),
(3, 'Tarjeta Debito');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plates`
--

CREATE TABLE `plates` (
  `plate_id` int(3) NOT NULL,
  `plate_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `plate_picture` varchar(250) COLLATE utf8_bin NOT NULL,
  `plate_price` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `plates`
--

INSERT INTO `plates` (`plate_id`, `plate_name`, `plate_picture`, `plate_price`) VALUES
(1, 'Pizza Pepperoni.', 'https://media.istockphoto.com/photos/oven-baked-pepperoni-pizza-picture-id1181651561?k=6&m=1181651561&s=612x612&w=0&h=MeRsDVECUkusfsbocHoLaWD0tmsLbBCr8r3EH-DAixw', 5000),
(2, 'Pizza Hawaiana.', 'https://cdn.queapetito.com/wp-content/uploads/2019/12/pizza-hawaiana-1-1200-amp.jpg', 5000),
(3, 'Hamburguesa Sencilla', 'https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/165384.jpg', 7000),
(4, 'Pizza Margherita - Grande.', 'https://www.toptravelitaly.com/assets/blog/images/6.jpg', 25000),
(5, 'Pizza Margherita - Mediana.', 'https://www.toptravelitaly.com/assets/blog/images/6.jpg', 15000),
(6, 'Pizza Margherita - Personal.', 'https://www.toptravelitaly.com/assets/blog/images/6.jpg', 5000),
(7, 'Pizza Pollo con champiñones - Personal.', 'https://bernnyspizza.co/wp-content/uploads/2019/05/Bernnys-gallery-11.jpg', 5000),
(8, 'Pizza Pollo con champiñones - Mediana.', 'https://bernnyspizza.co/wp-content/uploads/2019/05/Bernnys-gallery-11.jpg', 15000),
(9, 'Pizza Pollo con champiñones - Grande.', 'https://bernnyspizza.co/wp-content/uploads/2019/05/Bernnys-gallery-11.jpg', 25000),
(10, 'Pizza Pepperoni - Grande.', 'https://media.istockphoto.com/photos/oven-baked-pepperoni-pizza-picture-id1181651561?k=6&m=1181651561&s=612x612&w=0&h=MeRsDVECUkusfsbocHoLaWD0tmsLbBCr8r3EH-DAixw', 25000),
(11, 'Pizza Pepperoni - Mediana.', 'https://media.istockphoto.com/photos/oven-baked-pepperoni-pizza-picture-id1181651561?k=6&m=1181651561&s=612x612&w=0&h=MeRsDVECUkusfsbocHoLaWD0tmsLbBCr8r3EH-DAixw', 15000),
(12, 'Pizza Hawuaiana - Mediana.', 'https://cdn.queapetito.com/wp-content/uploads/2019/12/pizza-hawaiana-1-1200-amp.jpg', 15000),
(13, 'Pizza Hawuaiana - Grande.', 'https://cdn.queapetito.com/wp-content/uploads/2019/12/pizza-hawaiana-1-1200-amp.jpg', 25000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `role_id` int(2) NOT NULL,
  `role_name` varchar(15) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'Administrador'),
(2, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `statuses`
--

CREATE TABLE `statuses` (
  `status_id` int(6) NOT NULL,
  `status_name` varchar(11) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `statuses`
--

INSERT INTO `statuses` (`status_id`, `status_name`) VALUES
(1, 'Nuevo'),
(2, 'Confirmado'),
(3, 'Preparando'),
(4, 'Enviando'),
(5, 'Entregado'),
(6, 'Cancelado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user_id` int(3) NOT NULL,
  `user_nickname` varchar(50) COLLATE utf8_bin NOT NULL,
  `user_names` varchar(50) COLLATE utf8_bin NOT NULL,
  `user_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `user_phone` varchar(13) COLLATE utf8_bin NOT NULL,
  `user_address` varchar(100) COLLATE utf8_bin NOT NULL,
  `user_password` varchar(50) COLLATE utf8_bin NOT NULL,
  `user_role_id` int(4) NOT NULL DEFAULT 2
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `user_nickname`, `user_names`, `user_email`, `user_phone`, `user_address`, `user_password`, `user_role_id`) VALUES
(1, 'davidmoreno', 'David Moreno', 'david@email.com', '+57112354785', 'Calle 500, bogotá oeste sur', '123456', 1),
(2, 'mariaMartin', 'María Martín', 'maría@email.com', '+57112354778', 'Calle 200 al sur de Italia', '123456', 2),
(3, 'NikolaTesla', 'Nikola Tesla', 'nikola@tesla.com', '312578945', 'Calle 81 diagonal 55 norte', '123456', 2),
(16, 'ElonMusk', 'Elon Musk', 'elon@musk.com', '312578945', 'Calle 81 diagonal 54 norte', '123456', 2),
(17, 'pepito', 'pepito perez', 'pepito@gmail.com', '12315745', 'calle 150 al sur de italia', '123456', 2),
(18, 'AlanTuring', 'Alan Turing', 'alan@turing.com', '312578945', 'Calle 82 diagonal 54 norte', '123456', 2),
(19, 'JeffBezos', 'Jeff Bezos', 'jeff@bezos.com', '312578945', 'Calle 100 diagonal 51 norte', '123456', 2),
(20, 'SteveJobs', 'Steve Jobs', 'steve@jobs.com', '312578945', 'Calle 100 diagonal 100 norte', '123456', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_status_fk` (`order_status_id`),
  ADD KEY `order_payment_fk` (`order_payment_id`),
  ADD KEY `order_user_fk` (`order_user_id`);

--
-- Indices de la tabla `orders_has_plates`
--
ALTER TABLE `orders_has_plates`
  ADD KEY `order_fk` (`order_id`),
  ADD KEY `plate_fk` (`plate_id`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indices de la tabla `plates`
--
ALTER TABLE `plates`
  ADD PRIMARY KEY (`plate_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indices de la tabla `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`status_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_nickname` (`user_nickname`),
  ADD UNIQUE KEY `user_email` (`user_email`),
  ADD KEY `role_fk` (`user_role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `payment_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `plates`
--
ALTER TABLE `plates`
  MODIFY `plate_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `statuses`
--
ALTER TABLE `statuses`
  MODIFY `status_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `order_payment_fk` FOREIGN KEY (`order_payment_id`) REFERENCES `payments` (`payment_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_status_fk` FOREIGN KEY (`order_status_id`) REFERENCES `statuses` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_user_fk` FOREIGN KEY (`order_user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `orders_has_plates`
--
ALTER TABLE `orders_has_plates`
  ADD CONSTRAINT `order_fk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `plate_fk` FOREIGN KEY (`plate_id`) REFERENCES `plates` (`plate_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `role_fk` FOREIGN KEY (`user_role_id`) REFERENCES `roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
