const sequelize = require("./conexion");

const getAllUsers = async () => {
  const data = await sequelize.query(
    "SELECT user_id AS id, user_nickname AS nickname, user_email AS email, user_phone AS phone, user_address AS address, user_role_id AS role_id FROM users",
    {
      type: sequelize.QueryTypes.SELECT,
    }
  );
  return data;
};

const getUserByEmail = async (nickname) => {
  try {
    const user = await sequelize.query(
      "SELECT user_id AS id, user_nickname AS nickname, user_email AS email, user_password AS password, user_role_id AS roleId  FROM users WHERE user_email = :nickname OR user_nickname = :nickname",
      {
        replacements: { nickname },
        type: sequelize.QueryTypes.SELECT,
      }
    );
    if(user.length === 0){
      return null
    }
    return user;
  } catch (error) {
    console.log(error);
    return "No se ha incontrado la información.";
  }
};

const getUserById = async (userId) => {
  try {
    const user = await sequelize.query(
      "SELECT user_id AS id, user_nickname AS nickname, user_email AS email, user_phone AS phone, user_address AS address, roles.role_name AS role  FROM users INNER JOIN  roles ON  roles.role_id = users.user_role_id WHERE user_id = :userId",
      {
        replacements: { userId },
        type: sequelize.QueryTypes.SELECT,
      }
    );
    if(user.length === 0){
      return null
    }
    return user;
  } catch (error) {
    console.log(error);
    return "No se ha incontrado la información.";
  }
};

const createUser = async (userData) => {
  let msg = "";
  try {
    const columns = Object.keys(userData);
    const values = Object.values(userData).join("','");

    await sequelize.query(
      `INSERT INTO users (${columns}) VALUES ('${values}')`,
      {
        type: sequelize.QueryTypes.INSERT,
      }
    );

    return (msg = "Usuario creado exitosamente.");
  } catch (error) {
    const errorCode = error.parent.errno;

    switch (errorCode) {
      case 1062:
        msg = "El nickname se encuentra en uso.";
        break;

      default:
        msg = `Codigo de error: ${errorCode}`;
        break;
    }

    return msg;
  }
};

module.exports = {
  getAllUsers,
  getUserByEmail,
  getUserById,
  createUser,
};
