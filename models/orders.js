const sequelize = require('./conexion');

const tableName = 'orders'

const getAllOrders = async () =>{
  try {
    const returnData = await sequelize
    .query(`SELECT order_id AS numero, order_description AS descripcion, order_time AS hora, order_total AS pago, users.user_names AS usuario, users.user_address AS direccion, payments.payment_name AS metodoPago, statuses.status_name AS estado FROM ${tableName} INNER JOIN users ON orders.order_user_id = users.user_id INNER JOIN payments ON orders.order_payment_id = payments.payment_id INNER JOIN statuses ON orders.order_status_id  = statuses.status_id`,
    {
      type: sequelize.QueryTypes.SELECT
    });
    return returnData;

  } catch (error) {
    console.log(error);
    return 'Data no encontrada.'
  }
}

const getOrdersByUserId = async (userId) =>{
  try {
    const orders = await sequelize
    .query(`SELECT order_id AS numberOrder, order_description AS description, order_time AS time, order_total AS totalPrice, statuses.status_name AS status, payments.payment_name AS payment, users.user_names AS names, users.user_address AS address FROM orders INNER JOIN  statuses ON orders.order_status_id = statuses.status_id INNER JOIN payments ON orders.order_payment_id = payments.payment_id INNER JOIN users ON orders.order_user_id = users.user_id WHERE order_user_id = ${userId}`, {
      type: sequelize.QueryTypes.SELECT
    });

    if(orders.length >= 0){
      return orders
    }else{
      return null
    }
  } catch (error) {
    console.log(error);
    return 'No se ha encontrado ordenes para este usuario.'
  }
}

const insertOrder = async (orderData) =>{
  
  try {
    const arrColumns = Object.keys(orderData);
    const arrValues = Object.values(orderData).join("','");
    const insert = await sequelize
    .query(`INSERT INTO ${tableName} (${arrColumns}) VALUES ('${arrValues}')`,
    {
      type: sequelize.QueryTypes.INSERT
    });
    await insertOrdersHasPlates(insert[0], orderData.order_description);

    return 'La orden ha sido creada con exito'
  } catch (error) {
    console.log(error);
    return 'La orden no ha sido creada con exito.'
  }
}

const updateOrder = async (orderId, orderData) =>{
  try {
    const arrColumns = Object.keys(orderData);
    const arrValues = Object.values(orderData);
    const newObj = arrColumns.map((element, index) => `${element} = "${arrValues[index]}"`);

    await sequelize
    .query(`UPDATE ${tableName} SET ${newObj.toString()} WHERE order_id = ${orderId}`,
    {
      type: sequelize.QueryTypes.UPDATE
    });

    return 'La orden ha sido actualizada con exito.'
  } catch (error) {
    console.log(error);
    return 'La orden no ha sido actualziada.'
  }
}

const insertOrdersHasPlates = async (orderId, plateIds) =>{
  try {
    plateIds = plateIds.replace(/'/g,'"');
    plateIds = JSON.parse(plateIds);

    const orderData = plateIds.sort();

    const orders_has_plates = await orderData.map(async(plateId) =>{
      await sequelize
      .query(`INSERT INTO orders_has_plates (order_id, plate_id) VALUES (${orderId}, ${plateId})`,
      {
        type: sequelize.QueryTypes.INSERT
      });
    });

    await Promise.all(orders_has_plates);
    await countPlatesByOrder(orderId);

  } catch (error) {
    console.log(error);
    return 'Ha ocurrido un error al intentar crear la relación order_has_plates'
  }
}

const countPlatesByOrder = async (orderId) =>{
  try {
    const getData = await sequelize
    .query(`SELECT COUNT(orders_has_plates.plate_id) AS total, SUM(plates.plate_price) AS price, order_id, plates.plate_name FROM orders_has_plates INNER JOIN plates ON orders_has_plates.plate_id = plates.plate_id WHERE order_id = ${orderId} GROUP BY(plates.plate_name)`,
    {
      type: sequelize.QueryTypes.SELECT
    });

    let orderData = {
      order_description: '',
      order_total: 0

    };
    getData.forEach((order)=>{
      orderData.order_description += `${order.total}x ${order.plate_name} `
      orderData.order_total +=  parseInt(order.price)
    });

    await updateOrder(orderId, orderData);

  } catch (error) {
    console.log(error);
    return 'No se pudo actualizar los valores prices y description.'
  }

}

const deleteOrder = async(orderId) =>{

  try {
    await sequelize.query(`DELETE FROM ${tableName} WHERE order_id = ${orderId}`, 
      {
        type: sequelize.QueryTypes.DELETE
      }
    );
    return 'Orden eliminada con exito.'

  } catch (error) {
    console.log(error);
    return 'La orden no ha sido elimianda.'
  }

};

module.exports = {
  getAllOrders,
  getOrdersByUserId,
  insertOrder,
  updateOrder,
  deleteOrder
}
