const  sequelize = require('./conexion');

const tableName = 'plates'

const getAllPlates = async () =>{
  try {
    const allPlates = await sequelize
    .query(`SELECT plate_name AS name, plate_picture AS picture, plate_price AS price FROM ${tableName}`,
      {
        type: sequelize.QueryTypes.SELECT
      }
    );

    return allPlates
  } catch (error) {
    console.log(error);
    return 'Información no encontrada.'
  }
}

const createPlate = async (plateData) =>{
  try {
    const arrColumns = Object.keys(plateData);
    const arrValues = Object.values(plateData).join("','");

    await sequelize
    .query(`INSERT INTO ${tableName} (${arrColumns}) VALUES ('${arrValues}')`,
      {
        type: sequelize.QueryTypes.INSERT
      }
    );

    return 'El plato ha sido creado exitosamente.'
  } catch (error) {
    console.log(error);
    return 'El plato no ha sido creado.';
  }

}

const updatePlate = async (plateId, plateData) =>{
  try {
    const arrColumns = Object.keys(plateData);
    const arrValues = Object.values(plateData);
    const newObj = arrColumns.map((element, index) => `${element} = "${arrValues[index]}"`);

    await sequelize
    .query(`UPDATE ${tableName} SET ${newObj.toString()} WHERE plate_id = ${plateId}`,
      {
        type: sequelize.QueryTypes.UPDATE
      }
    );

      return 'El plato ha sido editado.'
  } catch (error) {
    console.log(error);
    return 'El plato no ha sido editado, debe enviar minimo un campo.'
  }


}

const deletePlate = async (plateId) =>{
  try {
    await sequelize.query(`DELETE FROM ${tableName} WHERE plate_id = ${plateId}`,
      { 
        type: sequelize.QueryTypes.DELETE
      }
    );

    return 'El palto ha sido eliminado correctamente.'
  } catch (error) {
    console.log(error);
    return 'El palto no ha sido eliminado correctamente.'
  }
};

module.exports = {
  getAllPlates,
  createPlate,
  updatePlate,
  deletePlate
}