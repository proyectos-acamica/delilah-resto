const  Sequelize  = require('sequelize');
require('dotenv').config();
const {DB_NAME, DB_HOST, DB_PASSWORD, DB_USER} = process.env

const sequelize = new Sequelize(DB_NAME || 'db_delilah', DB_USER || 'root', DB_PASSWORD || '', {
    host: DB_HOST || 'localhost',
    dialect: 'mysql'
  });
module.exports = sequelize;