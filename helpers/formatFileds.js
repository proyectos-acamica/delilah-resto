
const formatFiles = (tableName,data) =>{
  const arrKeys = Object.keys(data);
  const arrValues = Object.values(data);
  let objData = {};

  arrKeys.map((key, index)=>{
    objData[`${tableName}_${key}`] = arrValues[index];
  });

  return objData;
}

module.exports = {
  formatFiles
}