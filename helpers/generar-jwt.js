const jwt = require("jsonwebtoken");

const getToken = (uid = "", roleId = "") => {
  return new Promise((resolve, reject) => {
    
    const payload = { uid, roleId };

    jwt.sign( payload, process.env.JWT_SECRET,
      {
        expiresIn: "4h",
      },
      (error, token) => {
        if (error) {
          console.log(error);
          reject("No se pudo generar el token");
        } else {
          resolve(token);
        }
      }
    );
  });
};
  
module.exports = {
  getToken,
};
