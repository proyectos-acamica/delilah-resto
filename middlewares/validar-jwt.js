const { request, response } = require('express');
const jwt = require('jsonwebtoken');



const validateJWT = (req = request, res = response, next) =>{

  const token = req.header('Authorization');

  if(!token){
    return res.status(401).json({msg:'No se envio el token en la petición'});
  }

  try {
    const {uid, roleId} = jwt.verify(token, process.env.JWT_SECRET);
    req.uid = uid;
    req.roleId = roleId
    next();
  } catch (error) {
    console.log(error);
    res.status(401).json({msg: 'Token no válido'});
  }

}

module.exports = {
  validateJWT
}