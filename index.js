const express = require('express');
const morgan = require('morgan');
require('dotenv').config();


const app = express();
const {PORT} = process.env;

app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.use(morgan('dev'));


app.use('/auth', require('./routers/auth'));
app.use('/users', require('./routers/users'));
app.use('/plates', require('./routers/plates'));
app.use('/orders', require('./routers/orders'));

app.listen(PORT, () =>{
    console.log(`El servidor esta corriendo en el puerto ${PORT}`);
})