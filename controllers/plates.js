const {getAllPlates, createPlate, updatePlate, deletePlate} = require('../models/plates');
const {formatFiles} = require('../helpers/formatFileds');

const tableNameField = 'plate'

const index = async (req, res) =>{
  const plates = await getAllPlates();
  res.status(200).json(plates);
}

const store = async (req,res) =>{
  try {
    const returnData = await createPlate(formatFiles(tableNameField, req.body));
    res.status(201).json(returnData);
  } catch (error) {
    res.status(500).json(error);
  }
}

const edit = async (req, res) => {
  try {
    const {id} = req.params;
    const returnData = await updatePlate(id, formatFiles(tableNameField, req.body));
    res.status(200).json(returnData);
  } catch (error) {
    res.status(500).json(error);
  }
}

const remove = async (req, res) =>{
  try {
    const {id} = req.params;
    const returnData = await deletePlate(id);
    res.status(200).json(returnData);
  } catch (error) {
    res.status(500).json(error);
  }
}


module.exports = {
  index,
  store,
  edit,
  remove
}





