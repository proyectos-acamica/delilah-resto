const { formatFiles } = require('../helpers/formatFileds');
const {getAllUsers, getUserById, createUser}  =  require('../models/users');

const tableNameField = 'user'

const index = async (req, res) =>{
  try {
    let users
    if(req.roleId === 1){
      users = await getAllUsers();
    }else{
      users = await getUserById(req.uid);
    }
    res.json(users).status(200);
  } catch (error) {
    res.json("Error del servidor").status(500);
  }
}

const store = async (req, res) =>{
  const InsertUser = await createUser(formatFiles(tableNameField,req.body));
  res.status(200).json(InsertUser);
}


module.exports = {
  index,
  store
}