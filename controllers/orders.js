const {getAllOrders, getOrdersByUserId, insertOrder, updateOrder, deleteOrder} = require('../models/orders');
const {formatFiles} = require('../helpers/formatFileds');

const tableNameFiled = 'order'

const index = async (req, res) =>{
  try {
    let returnData
    if(req.roleId === 1){
      returnData = await getAllOrders();
    }else{
      returnData = await getOrdersByUserId(req.uid);
    }
    res.status(200).json(returnData);
  } catch (error) {
    console.log(error);
    res.status(500).json('No se encontró la información');
  }
}

const store = async (req, res) =>{
  try {
    const orderData = {
      ...req.body,
      user_id: req.uid
    }
    const returnData = await insertOrder(formatFiles(tableNameFiled, orderData));
    res.status(201).json(returnData);

  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
}

const edit = async (req, res) =>{
  try {
    const {id} = req.params;
    const returnData = await updateOrder(id, formatFiles(tableNameFiled, req.body));
    res.status(200).json(returnData);

  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
}

const remove = async (req, res) =>{
  try {
    const {id} = req.params;
    const returnData = await deleteOrder(id);
    res.status(200).json(returnData);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
}
module.exports = {
  index,
  store,
  edit,
  remove
}