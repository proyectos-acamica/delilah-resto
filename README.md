# Delilah Restó

Este es el proyecto del tercer bloque de la carrera: Desarrollo Web FullStack.

### Pre-requisitos 📋

Conocimientos en Node.js con Express.

## Construido con 🛠️

Estas fueron las herramientas usadas para el desarrollo de este proyecto:

* [Node.js](https://nodejs.org/en/) 
* [ExpressJs](https://expressjs.com/)
* [JWT](https://www.npmjs.com/package/jsonwebtoken)
* [Sequelize](https://sequelize.org/)
* [Swagger](https://swagger.io/) - Documentacón API.

## Versionado 📌

La indea es implementar [SemVer](http://semver.org/) para el versionado, por lo pronto se asigna la v0.0.1

## Licencia 📄

Este proyecto está bajo la Licencia  MIT.

## Demo 🖥

* [Delilah Restó](https://delila-resto.herokuapp.com/).
* [Documentación API](https://app.swaggerhub.com/apis-docs/DavidMorenoCode8/Delilah/1.0.0#).

## Colección Postman 🚀

* [Colección Delilah Restó](https://www.getpostman.com/collections/0f1053439b2f55c0d937).

## Instalación 📔

* #### Proyecto:
  * Clonar el repositorio.
  * ejecutar `npm install`.
  * renombrar el archivo `.env.example` por `.env`
  * ejecutar `npm run dev`.

* #### Base de datos:
  * instalar xampp.
  * iniciar Mysql.
  * crear base de datos: `db_delilah`.
  * Seleccionar la base de datos.
  * importar el `db_delilah.sql` .

* #### Postman:
  * Importar la colección ([Colección Delilah Restó](https://www.getpostman.com/collections/0f1053439b2f55c0d937)) - o la adjunta en el repositorio.

----
⌨️ por [David Moreno](https://www.linkedin.com/in/davidmorenocode/) 🤓☕
